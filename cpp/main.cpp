#include "header/header.h"


int main(int argc, char* argv[]){
    if(argc < 4 || argc % 2 != 0){
        Usage(argv[0]);
        return -1;
    }
    char* dev = argv[1]; //argv[1] = Interface
    Mac mymac;
    uint8_t myip[IP_LEN]={0,};

    if(get_mymac(dev, &mymac)==true && get_myip(dev, myip)==true){ //Get MyMAC && MyIP
        packet_relay(dev, &mymac, myip, argc, argv);
    }
}
