#include "header/header.h"

bool get_mymac(char *dev, Mac *mymac){
    struct ifreq ifr;
        int s;
        if ((s = socket(AF_INET, SOCK_STREAM,0)) < 0) {
            perror("socket");
            return false;
        }
        strcpy(ifr.ifr_name, dev);
        if (ioctl(s, SIOCGIFHWADDR, &ifr) < 0) {
            perror("ioctl");
            return false;
        }
        memcpy(&mymac[0], &ifr.ifr_hwaddr.sa_data[0], MAC_LEN);
        return true;
}

bool get_myip(char *dev, uint8_t myip[IP_LEN]) //Helped
{
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;

    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
        if (strcmp(ifa->ifa_name, dev) == 0)
        {
            if (ifa->ifa_addr->sa_family==AF_INET) {
                sa = (struct sockaddr_in *) ifa->ifa_addr;
                freeifaddrs(ifap);
                memcpy(&myip[0], &sa->sin_addr, IP_LEN);
                return true;
            }
        }
    }
    freeifaddrs(ifap);
    return false;
}

