#include "header/header.h"

int Usage(char *argv){
    printf("Usage : %s [Interface] [Sender IP] [Target IP] ... [Sender IP] [Target IP] ...\n", argv);
    printf("Example) ./ARP_Spoof eth0 192.168.0.11 192.168.0.1 ...\n");
    return -1;
}

void print_mac(u_char *mac){
    printf("%02X:%02X:%02X:%02X:%02X:%02X \n",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
}

void print_ip(uint8_t *ip){
    printf("%d.%d.%d.%d \n",ip[0],ip[1],ip[2],ip[3]);
}

void print_packet(const u_char *packet_read){
    printf("%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X \n",packet_read[0],packet_read[1],packet_read[2],packet_read[3],packet_read[4],packet_read[5],packet_read[6],packet_read[7],packet_read[8],packet_read[9],packet_read[10],packet_read[11],packet_read[12],packet_read[13],packet_read[14],packet_read[15]);
}
