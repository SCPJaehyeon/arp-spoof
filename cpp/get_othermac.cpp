#include "header/header.h"

bool get_othermac(char *dev, Mac *mymac, uint8_t myip[IP_LEN], uint32_t otherip, Mac *othermac){
    struct packet sendpacket;
    Mac broadMac; memset(&broadMac[0], 0xFF, MAC_LEN);
    Mac nothingMac; memset(&nothingMac[0], 0x00, MAC_LEN);
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    memcpy(&sendpacket.etherh.dstmac, &broadMac[0], MAC_LEN); //Destination MAC = BroadCast
    memcpy(&sendpacket.etherh.srcmac, &mymac[0], MAC_LEN); //Source MAC = MY
    sendpacket.etherh.type=ETYPE_ARP;sendpacket.arph.htype=HTYPE;sendpacket.arph.ptype=PTYPE;sendpacket.arph.hlen=HLEN;sendpacket.arph.prolen=PROLEN;sendpacket.arph.op=ARP_REQ;
    memcpy(&sendpacket.arph.senmac, &mymac[0], MAC_LEN); //Sender MAC = MY
    memcpy(&sendpacket.arph.senip, &myip[0], IP_LEN); //Sender IP = MY
    memcpy(&sendpacket.arph.tarmac, &nothingMac[0], MAC_LEN); //Target MAC = Nothing
    memcpy(&sendpacket.arph.tarip, &otherip, sizeof(otherip)); //Target IP

    int res = pcap_sendpacket(handle,(u_char*)&sendpacket, sizeof(packet)); //send ARP Request Packet
    if(res == -1){
        printf("ARP Request Packet Send Fail \n");
    }else{
        printf("ARP Request Packet Send Success \n");
    }
    while(true){
        struct pcap_pkthdr *header;
        const unsigned char *packet_read;
        int res1 = pcap_next_ex(handle, &header, &packet_read);
        if(res1==1){
            struct etherh *eth = (struct etherh*)(&packet_read[0]);
            if(eth->type==ETYPE_ARP && memcmp(&eth->dstmac, mymac, MAC_LEN)==0){ //check DestinationMAC==MYMAC && ARP Packet
                struct arph *arp = (struct arph*)(&packet_read[sizeof(etherh)]);
                if(arp->op == ARP_REP && memcmp(&arp->senip, &otherip, IP_LEN)==0){ //check SenderIP==ARGV[sen] && ARP REPLY Packet
                    printf("ARP Reply Packet Recived Success \n");
                    memcpy(&othermac[0], &packet_read[6], MAC_LEN);
                    pcap_close(handle);
                    return true;
                }
            }
        }else {
            printf("Target MAC Finding.. \n");
        }
    }
    printf("ARP Reply Packet Recived Fail");
    pcap_close(handle);
    return false;
}
