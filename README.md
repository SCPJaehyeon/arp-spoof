# arp-spoof

Joongbu University CCIT - Network



#### Usage

```shell
Usage:) ./arp-spoof <device> <sender ip 1> <target ip 1> [<sender ip 2> <target ip 2>...]
Example:) ./arp-spoof eth0 192.168.10.2 192.168.10.1 192.168.10.1 192.168.10.2
```