#pragma once
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pcap/pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ifaddrs.h>
#define ETYPE_IPV4 0x0008
#define ETYPE_IPV6 0xDD08
#define ETYPE_ARP 0x0608
#define HTYPE 0x0100
#define PTYPE 0x0008
#define HLEN  6
#define PROLEN  4
#define ARP_REQ 0x0100
#define ARP_REP 0x0200
#define MAC_LEN 6
#define IP_LEN  4
#define DUM_LEN 10000


class Mac{
protected:
    u_char mac[MAC_LEN];
public:
    Mac() {};
    ~Mac() {};
    operator u_char*() const{
        return (u_char*)mac;
    }
};

struct etherh { //Ethernet header
    Mac dstmac;
    Mac srcmac;
    uint16_t type;
};

struct arph { //ARP header
    uint16_t htype;
    uint16_t ptype;
    uint8_t hlen;
    uint8_t prolen;
    uint16_t op;
    Mac senmac;
    uint8_t senip[IP_LEN];
    Mac tarmac;
    uint8_t tarip[IP_LEN];
};

struct iph{ //IP header
    uint8_t verandh;
    uint8_t service;
    uint16_t totallength;
    uint16_t identi;
    uint16_t offset;
    uint8_t ttl;
    uint8_t pro;
    uint16_t checks;
    uint8_t sip[IP_LEN];
    uint8_t dip[IP_LEN];
};

struct packet { //ARP Packet
    struct etherh etherh;
    struct arph arph;
};

struct repacket { //IP Packet
    struct etherh etherh;
    struct iph iph;
    u_char dummy[DUM_LEN];
};

struct Flow{
    uint32_t senip;
    Mac senmac;
    uint32_t tarip;
    Mac tarmac;
};

//For Debug
int Usage(char *argv);
void print_mac(u_char *mac);
void print_ip(uint8_t *ip);
void print_packet(const u_char *packet_read);
//For GetInFo
bool get_mymac(char *dev, Mac *mymac);
bool get_myip(char *dev, uint8_t myip[IP_LEN]);
bool get_othermac(char *dev, Mac *mymac, uint8_t myip[IP_LEN], uint32_t otherip, Mac *othermac);
//For Attack
int arp_spoof(char *dev, Mac *mymac, Mac *tar_mac,uint32_t SenIP, uint32_t TarIP);
int packet_relay(char *dev, Mac *mymac, uint8_t myip[IP_LEN], int argc, char **argv);

