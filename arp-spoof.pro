TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpcap
SOURCES += \
    cpp/arpspoof.cpp \
    cpp/get_myaddr.cpp \
    cpp/get_othermac.cpp \
    cpp/main.cpp \
    cpp/packet_relay.cpp \
    cpp/print_info.cpp

HEADERS += \
    header/header.h

